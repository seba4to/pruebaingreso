package com.pruebaingreso.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import com.fasterxml.classmate.TypeResolver;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	@Autowired
	private TypeResolver typeResolver;

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apinfo()).select()
				.apis(RequestHandlerSelectors.basePackage("com.pruebaingreso.controller"))
				.paths(PathSelectors.any()).build();
	}

	private ApiInfo apinfo() {
		return new ApiInfoBuilder()
				.title("prueba de ingreso")
				.version("1.0-SNAPSHOT")
				.description("Servicios para prueba de ingreso")
				.contact(new Contact("Alvaro Maurelia",null,"amaurelia@udec.cl"))
				.build();
	}

}
