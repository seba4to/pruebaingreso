package com.pruebaingreso.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pruebaingreso.entity.ClienteEntity;
import com.pruebaingreso.entity.ProductoEntity;
import com.pruebaingreso.entity.QuantityEntity;
import com.pruebaingreso.service.IPruebaIngresoServices;

@Controller
@RequestMapping("producto")
public class ProductoController {
	
	@Autowired
	private IPruebaIngresoServices pruebaIngresoService;
	
	@GetMapping("contarProductos")
	public ResponseEntity<QuantityEntity> contarProductos( ) {
		QuantityEntity list = pruebaIngresoService.contarProductos();
		return new ResponseEntity<QuantityEntity>(list, HttpStatus.OK);
	}
	
	@GetMapping("obtenerProductos/{limitFirst}/{limitRows}")
	public ResponseEntity<List<ProductoEntity>> getAllProductos(
			@PathVariable("limitFirst") Integer limitFirst, 
			@PathVariable("limitRows") Integer limitRows) {
		List<ProductoEntity> cliente = pruebaIngresoService.getAllProductos(limitFirst, limitRows);
		return new ResponseEntity<List<ProductoEntity>>(cliente, HttpStatus.OK);
	}
	
	@GetMapping("comprarProducto/{clienteId}/{productoId}/")
	public ResponseEntity<QuantityEntity> comprarProducto(
			@PathVariable("clienteId") Integer clienteId, 
			@PathVariable("productoId") Integer productoId) {
		boolean response = pruebaIngresoService.comprarProducto(clienteId, productoId);
		return new ResponseEntity(response, HttpStatus.OK);
	}
	
	@GetMapping("comprarRecarga/{clienteId}/{compraRecarga}/")
	public ResponseEntity<QuantityEntity> comprarRecarga(
			@PathVariable("clienteId") Integer clienteId, 
			@PathVariable("compraRecarga") Integer compraRecarga) {
		boolean response = pruebaIngresoService.comprarRecarga(clienteId, compraRecarga);
		return new ResponseEntity(response, HttpStatus.OK);
	}


} 