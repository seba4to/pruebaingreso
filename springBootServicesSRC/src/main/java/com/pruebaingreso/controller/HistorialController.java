package com.pruebaingreso.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pruebaingreso.entity.ClienteEntity;
import com.pruebaingreso.entity.HistorialEntity;
import com.pruebaingreso.entity.QuantityEntity;
import com.pruebaingreso.service.IPruebaIngresoServices;

@Controller
@RequestMapping("historial")
public class HistorialController {
	
	@Autowired
	private IPruebaIngresoServices pruebaIngresoService;
	
	@GetMapping("obtenerHistorial/{limitFirst}/{limitRows}")
	public ResponseEntity<List<HistorialEntity>> obtenerHistorial(
			@PathVariable("limitFirst") Integer limitFirst, 
			@PathVariable("limitRows") Integer limitRows ) {
		List<HistorialEntity> cliente = pruebaIngresoService.obtenerHistorial(limitFirst,limitRows);
		return new ResponseEntity<List<HistorialEntity>>(cliente, HttpStatus.OK);
	}
	
	@GetMapping("contarHistorial")
	public ResponseEntity<QuantityEntity> contarHistorial( ) {
		QuantityEntity list = pruebaIngresoService.contarHistorial();
		return new ResponseEntity<QuantityEntity>(list, HttpStatus.OK);
	}

} 