package com.pruebaingreso.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pruebaingreso.entity.ClienteEntity;
import com.pruebaingreso.entity.HistorialEntity;
import com.pruebaingreso.service.IPruebaIngresoServices;
import com.pruebaingreso.entity.QuantityEntity;

@Controller
@RequestMapping("cliente")
public class ClienteController {
	
	@Autowired
	private IPruebaIngresoServices pruebaIngresoService;
	
	@GetMapping("obtenerClientes/{limitFirst}/{limitRows}/{userActive}")
	public ResponseEntity<List<ClienteEntity>> getAllClientes( 
			@PathVariable("limitFirst") Integer limitFirst, 
			@PathVariable("limitRows") Integer limitRows,  
			@PathVariable("userActive") Integer userActive ) {
		List<ClienteEntity> cliente = pruebaIngresoService.getAllClientes(limitFirst, limitRows, userActive);
		return new ResponseEntity<List<ClienteEntity>>(cliente, HttpStatus.OK);
	}
	
	@GetMapping("contarClientes/{userActive}")
	public ResponseEntity<QuantityEntity> contarClientes( @PathVariable("userActive") Integer userActive ) {
		QuantityEntity list = pruebaIngresoService.contarClientes(userActive);
		return new ResponseEntity<QuantityEntity>(list, HttpStatus.OK);
	}
	
	@GetMapping("cambiarNombre/{clienteId}/{clienteNombre}")
	public ResponseEntity<List<ClienteEntity>> editName(
			@PathVariable("clienteId") Integer clienteId, 
			@PathVariable("clienteNombre") String clienteNombre) {
		boolean response = pruebaIngresoService.editName(clienteId, clienteNombre);
		return new ResponseEntity(response, HttpStatus.OK);
	}
	
	@GetMapping("modificarSaldo/{clienteId}/{clienteSaldo}")
	public ResponseEntity<List<ClienteEntity>> modificarSaldo(
			@PathVariable("clienteId") Integer clienteId, 
			@PathVariable("clienteSaldo") String clienteSaldo) {
		boolean response = pruebaIngresoService.modificarSaldo(clienteId, clienteSaldo);
		return new ResponseEntity(response, HttpStatus.OK);
	}
	
} 