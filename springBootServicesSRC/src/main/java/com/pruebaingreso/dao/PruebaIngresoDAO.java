package com.pruebaingreso.dao;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


import com.pruebaingreso.entity.ClienteEntity;
import com.pruebaingreso.entity.ClienteRowMapper;
import com.pruebaingreso.entity.HistorialEntity;
import com.pruebaingreso.entity.HistorialRowMapper;
import com.pruebaingreso.entity.ProductoEntity;
import com.pruebaingreso.entity.ProductoRowMapper;
import com.pruebaingreso.entity.QuantityEntity;
import com.pruebaingreso.entity.QuantityRowMapper;
@Transactional
@Repository
public class PruebaIngresoDAO implements IPruebaIngresoDAO {
	
	@Autowired
    private JdbcTemplate jdbcTemplate;
	
	@Override
	public List<ClienteEntity> getAllClientes(Integer limitFirst, Integer limitRows, Integer userActive) {
		String sql = "SELECT * FROM clientes ";
		if(userActive==1) {
			sql += "WHERE clienteActivo = 1 ";
		}
		sql += "LIMIT " + limitFirst + "," + limitRows;	
		RowMapper<ClienteEntity> rowMapper = new ClienteRowMapper();
		return this.jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public List<ProductoEntity> getAllProductos(Integer limitFirst, Integer limitRows){
		String sql = "SELECT * FROM productos LIMIT " + limitFirst + "," + limitRows;
		RowMapper<ProductoEntity> rowMapper = new ProductoRowMapper();
		return this.jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public List<HistorialEntity> obtenerHistorial(Integer limitFirst, Integer limitRows) {
		String sql = "SELECT c.compraFecha, cl.clienteNombre, p.productoNombre, \r\n" + 
				"p.productoPrecio, c.compraRecarga\r\n" + 
				"FROM compras c\r\n" + 
				"LEFT JOIN clientes cl ON cl.clienteId = c.clienteId\r\n" + 
				"LEFT JOIN productos p ON p.productoId = c.productoId LIMIT "+limitFirst+","+limitRows;
		System.out.println("ALLSTAR: " + sql);
		RowMapper<HistorialEntity> rowMapper = new HistorialRowMapper();
		return this.jdbcTemplate.query(sql, rowMapper);
	}

	@Override
	public QuantityEntity contarClientes(Integer userActive) {
		String sql;
		if (userActive==1) {
			sql = "SELECT COUNT(*) AS cantidad FROM clientes WHERE clienteActivo = 1";
		}
		else {
			sql = "SELECT COUNT(*) AS cantidad FROM clientes";
		}
		RowMapper<QuantityEntity> rowMapper = new QuantityRowMapper();
		return jdbcTemplate.queryForObject(sql, rowMapper);
	}

	@Override
	public boolean editName(Integer clienteId, String clienteNombre) {
		String sql = "UPDATE clientes SET clienteNombre='" + clienteNombre + "' WHERE clienteId="
				+ clienteId + ";";
		this.jdbcTemplate.update(sql);
		return true;
	}

	@Override
	public QuantityEntity contarProductos() {
		String sql = "SELECT COUNT(*) AS cantidad FROM productos";
		RowMapper<QuantityEntity> rowMapper = new QuantityRowMapper();
		return jdbcTemplate.queryForObject(sql, rowMapper);
	}

	@Override
	public boolean modificarSaldo(Integer clienteId, String clienteSaldo) {
		String sql = "UPDATE clientes SET clienteSaldo='" + clienteSaldo + "' WHERE clienteId="
				+ clienteId + ";";
		this.jdbcTemplate.update(sql);
		return true;
	}

	@Override
	public boolean comprarProducto(Integer clienteId, Integer productoId) {
		
		String sql = "INSERT INTO compras (`clienteId`, `productoId`  ) " + "VALUES ('"
				+ clienteId + "', '" + productoId + "')";
		this.jdbcTemplate.update(sql);
		
		sql = "SELECT productoPrecio as cantidad FROM productos WHERE productoId=" + productoId;
		RowMapper<QuantityEntity> rowMapper = new QuantityRowMapper();
		QuantityEntity saldoGastado = jdbcTemplate.queryForObject(sql, rowMapper);

		sql = "SELECT clienteSaldo as cantidad FROM clientes WHERE clienteId=" + clienteId;
		QuantityEntity saldoActual = jdbcTemplate.queryForObject(sql, rowMapper);

		int nuevoSaldo = saldoActual.getCantidad() - saldoGastado.getCantidad();
		sql = "UPDATE clientes SET clienteSaldo='" + nuevoSaldo + "' WHERE clienteId="
				+ clienteId + ";";
		this.jdbcTemplate.update(sql);
		return true;
	}

	@Override
	public QuantityEntity contarHistorial() {
		String sql = "SELECT COUNT(*) AS cantidad FROM compras";
		RowMapper<QuantityEntity> rowMapper = new QuantityRowMapper();
		return jdbcTemplate.queryForObject(sql, rowMapper);
	}

	@Override
	public boolean comprarRecarga(Integer clienteId, Integer compraRecarga) {
		String sql = "INSERT INTO compras (`clienteId`, `compraRecarga`  ) " + "VALUES ('"
				+ clienteId + "', '" + compraRecarga + "')";
		this.jdbcTemplate.update(sql);
		return true;
	}

}
