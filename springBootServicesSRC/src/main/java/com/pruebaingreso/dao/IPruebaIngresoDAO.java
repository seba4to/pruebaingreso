package com.pruebaingreso.dao;
import java.util.List;

import com.pruebaingreso.entity.ClienteEntity;
import com.pruebaingreso.entity.HistorialEntity;
import com.pruebaingreso.entity.ProductoEntity;
import com.pruebaingreso.entity.QuantityEntity;

public interface IPruebaIngresoDAO {

    
	List<ClienteEntity> getAllClientes(Integer limitFirst, Integer limitRows, Integer userActive);

	List<ProductoEntity> getAllProductos(Integer limitFirst, Integer limitRows);

	List<HistorialEntity> obtenerHistorial(Integer limitFirst, Integer limitRows);

	QuantityEntity contarClientes(Integer userActive);

	boolean editName(Integer clienteId, String clienteNombre);

	QuantityEntity contarProductos();

	boolean modificarSaldo(Integer clienteId, String clienteSaldo);

	boolean comprarProducto(Integer clienteId, Integer productoId);

	QuantityEntity contarHistorial();

	boolean comprarRecarga(Integer clienteId, Integer compraRecarga);
}
 