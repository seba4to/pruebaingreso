package com.pruebaingreso.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pruebaingreso.dao.IPruebaIngresoDAO;
import com.pruebaingreso.entity.ClienteEntity;
import com.pruebaingreso.entity.HistorialEntity;
import com.pruebaingreso.entity.ProductoEntity;
import com.pruebaingreso.entity.QuantityEntity;
@Service
public class PruebaIngresoService implements IPruebaIngresoServices {
	
	@Autowired
	private IPruebaIngresoDAO pruebaIngresoDAO;
	
	
	
	@Override
	public List<ClienteEntity> getAllClientes(Integer limitFirst, Integer limitRows, Integer userActive) {
		return pruebaIngresoDAO.getAllClientes(limitFirst, limitRows, userActive);
	}



	@Override
	public List<HistorialEntity> obtenerHistorial(Integer limitFirst, Integer limitRows) {
		return pruebaIngresoDAO.obtenerHistorial(limitFirst,limitRows);
	}



	@Override
	public QuantityEntity contarClientes(Integer userActive) {
		return pruebaIngresoDAO.contarClientes(userActive);
	}



	@Override
	public boolean editName(Integer clienteId, String clienteNombre) {
		return pruebaIngresoDAO.editName(clienteId, clienteNombre);
	}



	@Override
	public List<ProductoEntity> getAllProductos(Integer limitFirst, Integer limitRows) {
		return pruebaIngresoDAO.getAllProductos(limitFirst, limitRows);
	}



	@Override
	public QuantityEntity contarProductos() {
		return pruebaIngresoDAO.contarProductos();

	}



	@Override
	public boolean modificarSaldo(Integer clienteId, String clienteSaldo) {
		return pruebaIngresoDAO.modificarSaldo(clienteId, clienteSaldo);
	}



	@Override
	public boolean comprarProducto(Integer clienteId, Integer productoId) {
		return pruebaIngresoDAO.comprarProducto(clienteId, productoId);
	}



	@Override
	public QuantityEntity contarHistorial() {
		return pruebaIngresoDAO.contarHistorial();
	}



	@Override
	public boolean comprarRecarga(Integer clienteId, Integer compraRecarga) {
		return pruebaIngresoDAO.comprarRecarga(clienteId, compraRecarga);
	}
	
	
	
	
}
