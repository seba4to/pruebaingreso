package com.pruebaingreso.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class HistorialRowMapper implements RowMapper<HistorialEntity> {

	@Override
	public HistorialEntity mapRow(ResultSet row, int rowNum) throws SQLException {
		
		HistorialEntity historial = new HistorialEntity();
		
		historial.setClienteNombre(row.getString("clienteNombre"));
		historial.setProductoNombre(row.getString("productoNombre"));
		historial.setCompraFecha(row.getString("compraFecha"));
		historial.setCompraRecarga(row.getInt("compraRecarga"));
		historial.setProductoPrecio(row.getInt("productoPrecio"));
		
		return historial;
		
	}

}
