package com.pruebaingreso.entity;

public class ClienteEntity { 
	
    private int clienteId;  
    private String clienteNombre;
    private int clienteSaldo;
    private int clienteActivo;
	
	public int getClienteId() {
		return clienteId;
	}
	public String getClienteNombre() {
		return clienteNombre;
	}
	public int getClienteSaldo() {
		return clienteSaldo;
	}
	public int getClienteActivo() {
		return clienteActivo;
	}
	
	public void setClienteId(int clienteId) {
		this.clienteId = clienteId;
	}
	public void setClienteNombre(String clienteNombre) {
		this.clienteNombre = clienteNombre;
	}
	public void setClienteSaldo(int clienteSaldo) {
		this.clienteSaldo = clienteSaldo;
	}
	public void setClienteActivo(int clienteActivo) {
		this.clienteActivo = clienteActivo;
	}
	
} 