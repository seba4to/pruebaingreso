package com.pruebaingreso.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class QuantityRowMapper implements RowMapper<QuantityEntity> {

	@Override
	public QuantityEntity mapRow(ResultSet row, int rowNum) throws SQLException {
		
		QuantityEntity workflow = new QuantityEntity();
		workflow.setCantidad(row.getInt("cantidad"));
		return workflow;
	}
	
}
