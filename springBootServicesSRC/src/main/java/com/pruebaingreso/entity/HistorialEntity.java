package com.pruebaingreso.entity;

public class HistorialEntity { 
	
	private String clienteNombre;
	private String productoNombre;
	private int productoPrecio;
	private String compraFecha;
	private int compraRecarga;
	
	public String getClienteNombre() {
		return clienteNombre;
	}
	public String getProductoNombre() {
		return productoNombre;
	}
	public String getCompraFecha() {
		return compraFecha;
	}
	public int getCompraRecarga() {
		return compraRecarga;
	}
	public int getProductoPrecio() {
		return productoPrecio;
	}
	
	public void setClienteNombre(String clienteNombre) {
		this.clienteNombre = clienteNombre;
	}
	public void setProductoNombre(String productoNombre) {
		this.productoNombre = productoNombre;
	}
	public void setCompraFecha(String compraFecha) {
		this.compraFecha = compraFecha;
	}
	public void setCompraRecarga(int compraRecarga) {
		this.compraRecarga = compraRecarga;
	}
	public void setProductoPrecio(int productoPrecio) {
		this.productoPrecio = productoPrecio;
	}
	
} 