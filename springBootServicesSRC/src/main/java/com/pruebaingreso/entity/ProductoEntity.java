package com.pruebaingreso.entity;

public class ProductoEntity { 
	
    private int productoId;  
    private String productoNombre;
    private int productoPrecio;  
	
	public int getProductoId() {
		return productoId;
	}
	public String getProductoNombre() {
		return productoNombre;
	}
	public int getProductoPrecio() {
		return productoPrecio;
	}
	
	public void setProductoId(int productoId) {
		this.productoId = productoId;
	}
	public void setProductoNombre(String productoNombre) {
		this.productoNombre = productoNombre;
	}
	public void setProductoPrecio(int productoPrecio) {
		this.productoPrecio = productoPrecio;
	}
	
} 