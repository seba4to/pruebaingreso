package com.pruebaingreso.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ProductoRowMapper implements RowMapper<ProductoEntity> {

	@Override
	public ProductoEntity mapRow(ResultSet row, int rowNum) throws SQLException {
		
		ProductoEntity producto = new ProductoEntity();
		
		producto.setProductoId(row.getInt("productoId"));
		producto.setProductoNombre(row.getString("productoNombre"));
		producto.setProductoPrecio(row.getInt("productoPrecio"));
		return producto;
		
	}

}
