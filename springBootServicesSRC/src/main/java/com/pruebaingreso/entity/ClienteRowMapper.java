package com.pruebaingreso.entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class ClienteRowMapper implements RowMapper<ClienteEntity> {

	@Override
	public ClienteEntity mapRow(ResultSet row, int rowNum) throws SQLException {
		
		ClienteEntity cliente = new ClienteEntity();
		
		cliente.setClienteId(row.getInt("clienteId"));
		cliente.setClienteNombre(row.getString("clienteNombre"));
		cliente.setClienteSaldo(row.getInt("clienteSaldo"));
		cliente.setClienteActivo(row.getInt("clienteActivo"));
		
		return cliente;
		
	}

}
