package com.pruebaingreso.entity;



public class QuantityEntity {
	
	private int cantidad;
	
	// get the variables ( the name mapped in the response is the name of the get+NAME, i.e. getWFN gives WFN in the map )
	public int getCantidad() {
		return cantidad;
	}
	
	// set the variables 
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
}
