-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         10.3.16-MariaDB - mariadb.org binary distribution
-- SO del servidor:              Win64
-- HeidiSQL Versión:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Volcando estructura de base de datos para pruebaingreso
CREATE DATABASE IF NOT EXISTS `pruebaingreso` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `pruebaingreso`;

-- Volcando estructura para tabla pruebaingreso.clientes
CREATE TABLE IF NOT EXISTS `clientes` (
  `clienteId` int(11) NOT NULL AUTO_INCREMENT,
  `clienteNombre` varchar(100) DEFAULT NULL,
  `clienteApellido` varchar(100) DEFAULT NULL,
  `clienteSaldo` int(11) DEFAULT NULL,
  `clienteActivo` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`clienteId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla pruebaingreso.clientes: ~7 rows (aproximadamente)
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` (`clienteId`, `clienteNombre`, `clienteApellido`, `clienteSaldo`, `clienteActivo`) VALUES
	(1, 'Alvaro', 'Maurelia', 7200, 1),
	(2, 'Eduardo', 'Padilla', 1500, 1),
	(3, 'Bárbara', 'Salfate', 8000, 0),
	(4, 'Nina', 'Lowell', 2500, 1),
	(5, 'Carlos', 'Mella', 7500, 1),
	(6, 'Mario', 'Neira', 3000, 0),
	(7, 'Felipe', 'Jara', 2500, 1);
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;

-- Volcando estructura para tabla pruebaingreso.compras
CREATE TABLE IF NOT EXISTS `compras` (
  `compraId` int(11) NOT NULL AUTO_INCREMENT,
  `compraFecha` date DEFAULT curdate(),
  `clienteId` int(11) DEFAULT NULL,
  `productoId` int(11) DEFAULT NULL,
  `compraRecarga` int(11) DEFAULT 0,
  PRIMARY KEY (`compraId`),
  KEY `FK_historialCompras_productos` (`productoId`),
  KEY `FK_historialCompras_clientes` (`clienteId`),
  CONSTRAINT `FK_historialCompras_clientes` FOREIGN KEY (`clienteId`) REFERENCES `clientes` (`clienteId`),
  CONSTRAINT `FK_historialCompras_productos` FOREIGN KEY (`productoId`) REFERENCES `productos` (`productoId`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla pruebaingreso.compras: ~25 rows (aproximadamente)
/*!40000 ALTER TABLE `compras` DISABLE KEYS */;
INSERT INTO `compras` (`compraId`, `compraFecha`, `clienteId`, `productoId`, `compraRecarga`) VALUES
	(1, '2020-06-06', 1, 3, 0),
	(2, '2020-06-06', 1, 1, 0),
	(3, '2020-06-05', 3, 2, 0),
	(4, '2020-06-06', 1, NULL, 5000),
	(5, '2020-06-01', 1, NULL, 8000),
	(6, '2020-06-01', 2, NULL, 1000),
	(12, '2020-06-07', 1, 1, 0),
	(13, '2020-06-07', 1, 1, 0),
	(14, '2020-06-07', 1, 1, 0),
	(15, '2020-06-07', 1, 2, 0),
	(16, '2020-06-07', 1, 5, 0),
	(17, '2020-06-07', 1, 4, 0),
	(18, '2020-06-07', 1, 5, 0),
	(19, '2020-06-07', 1, 5, 0),
	(20, '2020-06-07', 1, 4, 0),
	(21, '2020-06-07', 1, 4, 0),
	(22, '2020-06-07', 5, 4, 0),
	(23, '2020-06-07', 7, NULL, 1000),
	(24, '2020-06-07', 7, NULL, 1000),
	(25, '2020-06-07', 7, NULL, 500),
	(26, '2020-06-07', 1, 1, 0),
	(27, '2020-06-07', 1, 5, 0),
	(28, '2020-06-07', 1, 3, 0),
	(29, '2020-06-07', 1, NULL, 6000),
	(30, '2020-06-07', 1, 2, 0);
/*!40000 ALTER TABLE `compras` ENABLE KEYS */;

-- Volcando estructura para tabla pruebaingreso.productos
CREATE TABLE IF NOT EXISTS `productos` (
  `productoId` int(11) NOT NULL AUTO_INCREMENT,
  `productoNombre` varchar(100) DEFAULT NULL,
  `productoPrecio` int(11) DEFAULT NULL,
  PRIMARY KEY (`productoId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Volcando datos para la tabla pruebaingreso.productos: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `productos` DISABLE KEYS */;
INSERT INTO `productos` (`productoId`, `productoNombre`, `productoPrecio`) VALUES
	(1, 'Coca Cola', 1200),
	(2, 'Papas', 800),
	(3, 'Galletas', 500),
	(4, 'Queso', 2000),
	(5, 'Piña', 300),
	(6, 'Jugo', 1400),
	(7, 'Pan', 700),
	(8, 'Helado', 1800),
	(9, 'Pizza', 2500),
	(10, 'Chocolates', 3500),
	(11, 'Jalea', 300);
/*!40000 ALTER TABLE `productos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
