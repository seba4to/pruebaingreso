# Servicios para "PruebaIngreso"

Proyecto Springboot con servicios REST que son consumidos por el webApp PruebaIngreso

## Get Started

Al ser un proyecto maven es necesario ejecutar `mvn clean install` para descargar las dependencias.

Para iniciar el proyecto hay que correr `PruebaIngresoApplication.java` como un Spring Boot App.

Los servicios escuchan en `http://localhost:8080/`. 

Para probarlos es posible acceder al swagger en `http://localhost:8080/swagger-ui.html`

## Pruebas Unitarias

Por temas de tiempo el proyecto no cuenta con pruebas unitarias aún.

## Base de datos

Para las consultas se utiliza una base de datos relacional.

En src/main/resources se encuentra un DUMP con datos de prueba.

La app busca la base de datos en `localhost:3306/pruebaingreso` con username=root y sin password.

Para cambiar las credenciales hay que modificar `main/resources/application.properties`

## Autores

Alvaro Maurelia - amaurelia@udec.cl - Desarrollo Inicial