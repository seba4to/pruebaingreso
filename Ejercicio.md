### Get Started

El sistema cuenta con 2 componentes, el webapp en angular ubicado en pruebaIngresoAngular y los servicios Springboot ubicados en springBootServicesSRC

El webApp se ejecuta con el comando mpn start y los servicios ejecutando una aplicación Springboot en PruebaIngresoApplication. Cada carpeta tiene un readme con más información.

Un DUMP de la base de datos se encuentra en springBootServicesSRC\src\main\resources

### Autor

Alvaro Maurelia - amaurelia@udec.cl -  Desarrollo Inicial