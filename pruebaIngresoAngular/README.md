# webApp "PruebaIngreso"

Proyecto Angular 8 para cumplir con los casos de uso de la prueba de ingreso planteada

## Get Started

Ejecutar `mpn install` para descargar los módulos.

El proyecto fue realizado con PrimeNG

`npm install primeng --save`

`npm install primeicons --save`

Para ejecutar el aplicativo ejecutar `mpn start`. Este escuchará en `http://localhost:4200/`.

## Pre - requisitos para ejecutar el programa

Node JS

Prime NG

Angular Cli

## Pruebas Unitarias

Por temas de tiempo el proyecto no cuenta con pruebas unitarias aún.

## Autores

Alvaro Maurelia - amaurelia@udec.cl - Desarrollo Inicial