import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClienteComponent } from './cliente/cliente.component';
import { ComprasComponent } from './compras/compras.component';
import { RecargaComponent } from './recarga/recarga.component';
import { HistoricoComponent } from './historico/historico.component';

const routes: Routes = [
  { path: '', component: ClienteComponent },
  { path: 'clientes', component: ClienteComponent },
  { path: 'compras', component: ComprasComponent },
  { path: 'recarga', component: RecargaComponent },
  { path: 'historico', component: HistoricoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [ 
  ClienteComponent, 
  ComprasComponent, 
  RecargaComponent, 
  HistoricoComponent ];
