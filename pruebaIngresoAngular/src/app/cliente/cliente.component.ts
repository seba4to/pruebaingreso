import { Component, OnInit } from '@angular/core';
import { Cliente } from '../modelos/cliente';
import { ClientesService } from '../servicios/clientes.service';
import {MessageService} from 'primeng/api';


@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {

  clientes: Cliente[];
  cantidadClientes: number;

  constructor(
    public clienteService: ClientesService,
    private messageService: MessageService) { }

  cargarClientes(event){
    this.clienteService.obtenerClientes(event.first, event.rows, 0).subscribe((response) => {
      this.clientes = <Cliente[]> response;
    });
  }

  cambiarNombre(id, nombre){
    this.clienteService.cambiarNombreCliente(id, nombre).subscribe((response) => {
      console.log(response);
      this.showSuccess("Nombre modificado satisfactoriamente");
    });
  }

  showSuccess(mensaje) {
    this.messageService.add({severity:'success', summary:"Mensaje", detail:mensaje});
  }

  ngOnInit() {
    this.clienteService.contarClientes(0).subscribe((response) => {
      this.cantidadClientes = response["cantidad"];
    });
  }

}
