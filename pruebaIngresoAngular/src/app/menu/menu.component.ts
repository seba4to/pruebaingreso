import { Component, OnInit } from '@angular/core';
import {MenuItem} from 'primeng/api';


@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor() { }

  
  items: MenuItem[];


  ngOnInit() {

    this.items = [
      {label: 'Cliente', icon: 'pi pi-fw pi-user', routerLink: ['/clientes']},
      {label: 'Compras', icon: 'pi pi-fw pi-money-bill', routerLink: ['/compras']},
      {label: 'Recarga', icon: 'pi pi-fw pi-sort-up', routerLink: ['/recarga']},
      {label: 'Histórico', icon: 'pi pi-fw pi-paperclip', routerLink: ['/historico']}
  ];

  }

}
