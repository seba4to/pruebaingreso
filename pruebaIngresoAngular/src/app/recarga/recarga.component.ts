import { Component, OnInit } from '@angular/core';
import { Cliente } from '../modelos/cliente';
import { ClientesService } from '../servicios/clientes.service';
import { ProductosService } from '../servicios/productos.service';
import {MessageService} from 'primeng/api';

@Component({
  selector: 'app-recarga',
  templateUrl: './recarga.component.html',
  styleUrls: ['./recarga.component.css']
})
export class RecargaComponent implements OnInit {

  clientes: Cliente[];
  cantidadClientes: number;
  SalgoPorAgregar:number;
  display: boolean = false;
  tituloModal: string;
  clienteId: number;
  clienteSaldo: number;
  first: number;
  rows : number;

  constructor(
    public productosService: ProductosService,
    public clienteService: ClientesService,
    private messageService: MessageService ) { }



  cargarClientes(event){
    this.first = event.first;
    this.rows = event.rows;
    this.clienteService.obtenerClientes(this.first, this.rows, 1).subscribe((response) => {
      this.clientes = <Cliente[]> response;
    });
  }

  ngOnInit() {
    this.clienteService.contarClientes(1).subscribe((response) => {
      this.cantidadClientes = response["cantidad"];
    });
  }

  abrirModalsaldo(id, clienteNombre, clienteSaldo){
    this.tituloModal = "Agregar saldo al cliente " + clienteNombre;
    this.clienteId = id; 
    this.clienteSaldo = clienteSaldo;
    this.SalgoPorAgregar = 0;
    this.display = true;
  }

  showSuccess(mensaje) {
    this.messageService.add({severity:'success', summary:"Mensaje", detail:mensaje});
  }

  agregarSaldo(){
    console.log("agregarSaldo");
    var nuevoSaldo:number = Number(this.SalgoPorAgregar) + Number(this.clienteSaldo);

    this.clienteService.modificarSaldoCliente(this.clienteId, nuevoSaldo).subscribe((response) => {
      console.log(response);
      this.clienteService.obtenerClientes(this.first, this.rows, 1).subscribe((response) => {
        this.clientes = <Cliente[]> response;

        this.productosService .comprarRecarga(this.clienteId, this.SalgoPorAgregar).subscribe((response) => {
        });


      });
      this.showSuccess("Saldo modificado satisfactoriamente");
      
    });


  }

}
