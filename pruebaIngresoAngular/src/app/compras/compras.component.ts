import { Component, OnInit } from '@angular/core';
import { Producto } from '../modelos/producto';
import { Cliente } from '../modelos/cliente';
import { ProductosService } from '../servicios/productos.service';
import {MessageService} from 'primeng/api';
import { ClientesService } from '../servicios/clientes.service';

@Component({
  selector: 'app-compras',
  templateUrl: './compras.component.html',
  styleUrls: ['./compras.component.css']
})
export class ComprasComponent implements OnInit {

  productos: Producto[];
  cantidadProductos:number;
  clientes: Cliente[];
  clienteEscogido: Cliente;

  constructor(
    public productoService: ProductosService,
    public clienteService: ClientesService,
    private messageService: MessageService ) { }

  comprar(clienteId, productoId){
    this.productoService.comprarProducto(clienteId, productoId).subscribe((response) => {
      this.showSuccess("Compra realizada satisfactoriamente");
      this.cargarPrimeros20Clientes();
      this.clienteEscogido = {
        clienteId: 0,
        clienteNombre: "",
        clienteSaldo: 0,
        clienteActivo: true
      }
    });
  }  

  cargarPrimeros20Clientes(){
    this.clienteService.obtenerClientes(0, 20, 1).subscribe((response) => {
      this.clientes = <Cliente[]> response;
    });
  }

  cargarProductos(event){
    this.productoService.obtenerProductos(event.first, event.rows).subscribe((response) => {
      this.productos = <Producto[]> response;
    });
  }

  showSuccess(mensaje) {
    this.messageService.add({severity:'success', summary:"Mensaje", detail:mensaje});
  }

  ngOnInit() {
    this.clienteEscogido = {
      clienteId: 0,
      clienteNombre: "",
      clienteSaldo: 0,
      clienteActivo: true
    }
    this.productoService.contarProductos().subscribe((response) => {
      console.log(response);
      this.cantidadProductos = response["cantidad"];
    });
    this.cargarPrimeros20Clientes();
  }

}
