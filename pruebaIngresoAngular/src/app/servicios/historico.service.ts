import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { Historico } from '../modelos/historico';
import { HttpClient} from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HistoricoService {

  constructor(private httpClient: HttpClient) { }

  data:Historico[];

  obtenerHistorial(limitFirst = 0, limitRows = 0){
    return this.httpClient.get<Historico[]>("http://localhost:8080/historial/obtenerHistorial/"+limitFirst+"/"+limitRows+"/").pipe(
      map((body: Historico[]) => body ),
      catchError( () => of('Hubo un error al obtener los historicos') )
    )
  }

  contarHistorial(){
    return this.httpClient.get("http://localhost:8080/historial/contarHistorial/").pipe(
      map((body) => body ),
      catchError( () => of('Hubo un error al contar los productos') )
    )
  }
}
