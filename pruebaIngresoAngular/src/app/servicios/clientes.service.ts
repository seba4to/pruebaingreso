import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http'
import { environment } from 'src/environments/environment.prod';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Cliente } from '../modelos/cliente';

import { HttpErrorResponse } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  data:Cliente[];

  constructor(private httpClient: HttpClient) { }

  modificarSaldoCliente(id, saldo){
    return this.httpClient.get("http://localhost:8080/cliente/modificarSaldo/"+id+"/"+saldo).pipe(
      map((body: Cliente[]) => body ),
      catchError( () => of('Hubo un error al cambiar el nombre') )
    )
  }

  cambiarNombreCliente(id, nombre){
    return this.httpClient.get<Cliente[]>("http://localhost:8080/cliente/cambiarNombre/"+"/"+id+"/"+nombre+"/").pipe(
      map((body: Cliente[]) => body ),
      catchError( () => of('Hubo un error al cambiar el nombre') )
    )
  }

  obtenerClientes(limitFirst = 0, limitRows = 0, userActive = 0){
    return this.httpClient.get<Cliente[]>("http://localhost:8080/cliente/obtenerClientes/"+limitFirst+"/"+limitRows+"/"+userActive+"/").pipe(
      map((body: Cliente[]) => body ),
      catchError( () => of('Hubo un error al obtener los clientes') )
    )
  }

  contarClientes(userActive = 0){
    return this.httpClient.get("http://localhost:8080/cliente/contarClientes/"+userActive+"/").pipe(
      map((body) => body ),
      catchError( () => of('Hubo un error al contar los clientes') )
    )
  }
  

}
