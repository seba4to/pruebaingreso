import { Injectable } from '@angular/core';
import { map, catchError } from 'rxjs/operators';
import { Producto } from '../modelos/producto';
import { HttpClient} from '@angular/common/http'
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(private httpClient: HttpClient) { }

  data:Producto[];

  comprarProducto(clienteId, productoId){
    return this.httpClient.get("http://localhost:8080/producto/comprarProducto/"+clienteId+"/"+productoId+"/").pipe(
      map((body) => body ),
      catchError( () => of('Hubo un error al comprar el producto') )
    )
  }

  obtenerProductos(limitFirst = 0, limitRows = 0){
    return this.httpClient.get<Producto[]>("http://localhost:8080/producto/obtenerProductos/"+limitFirst+"/"+limitRows+"/").pipe(
      map((body: Producto[]) => body ),
      catchError( () => of('Hubo un error al obtener los productos') )
    )
  }

  contarProductos(){
    return this.httpClient.get("http://localhost:8080/producto/contarProductos/").pipe(
      map((body) => body ),
      catchError( () => of('Hubo un error al contar los productos') )
    )
  }

  comprarRecarga(clienteId, compraRecarga){
    return this.httpClient.get("http://localhost:8080/producto/comprarRecarga/"+clienteId+"/"+compraRecarga+"/").pipe(
      map((body) => body ),
      catchError( () => of('Hubo un error al comprar la recarga') )
    )
  }

}
