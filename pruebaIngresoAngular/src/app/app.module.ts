import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComprasComponent } from './compras/compras.component';
import { RecargaComponent } from './recarga/recarga.component';
import { HistoricoComponent } from './historico/historico.component';

import {TableModule} from 'primeng/table';
import {MenuModule} from 'primeng/menu';
import {TabMenuModule} from 'primeng/tabmenu';

import { ClientesService } from './servicios/clientes.service';
import { ProductosService } from './servicios/productos.service';

import { HttpClientModule } from '@angular/common/http';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MenuComponent } from './menu/menu.component';
import {ButtonModule} from 'primeng/button';
import { FormsModule } from '@angular/forms';
import {ToastModule} from 'primeng/toast';
import {MessageService} from 'primeng/api';

import {DialogModule} from 'primeng/dialog';
import {DropdownModule} from 'primeng/dropdown';

@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    ComprasComponent,
    RecargaComponent,
    HistoricoComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TableModule,
    HttpClientModule,
    MenuModule,
    BrowserAnimationsModule,
    TabMenuModule,
    ButtonModule,
    FormsModule,
    ToastModule,
    DialogModule,
    DropdownModule
  ],
  providers: [ClientesService, ProductosService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
