import { Component, OnInit } from '@angular/core';
import { HistoricoService } from '../servicios/historico.service';
import { Historico } from '../modelos/historico';

@Component({
  selector: 'app-historico',
  templateUrl: './historico.component.html',
  styleUrls: ['./historico.component.css']
})
export class HistoricoComponent implements OnInit {

  constructor(
    public historicoService: HistoricoService
  ) { }
  
  cantidadHistoricos: number;
  historicos: Historico[];

  cargarHistorial(event){
    console.log("loading varga historial");
    this.historicoService.obtenerHistorial(event.first, event.rows).subscribe((response) => {
      this.historicos = <Historico[]> response;
    });
  }

  ngOnInit() {
    this.historicoService.contarHistorial().subscribe((response) => {
      this.cantidadHistoricos = response["cantidad"];
    });
  }

}
